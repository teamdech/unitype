### UniType ###

UniType allows you to quickly enter any Unicode character. Simply press ALT and + to open the input window, enter the character number and press ENTER. Cancel by pressing ESCAPE or clicking somewhere else.